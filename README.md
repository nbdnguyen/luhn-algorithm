# Luhn Algorithm

A console app that runs the Luhn algorithm once for a user submitted input number. 

Input a whole number to test.

![run example](resources/luhn.png?raw=true "Title")

Contains: 

    - Main.java: Runs the console app.

    - Input.java: Takes a user input and divides it into a number and a check number.

    - Luhn.java: Takes an array and calculates the check number from the number. And checks if the submitted check number equals the calculated check number.
    
    - LuhnTest.java: 10 different unit tests for the application. 