package main.test;

import main.java.Input;
import main.java.Luhn;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnTest {
    long[] arr = new long[2];
    Luhn luhn = new Luhn(arr);

    @Test
    public void testFalse() {
        arr[0] = 1; arr[1] = 1;
        luhn.newNumbers(arr);
        assertFalse(luhn.validate());
    }

    @Test
    public void testTrue() {
        arr[0] = 1; arr[1] = 8;
        luhn.newNumbers(arr);
        assertTrue(luhn.validate());
    }

    @Test
    public void testFalseCredit() {
        arr[0] = 123456789012345L;
        luhn.newNumbers(arr);
        assertFalse(luhn.validate());
    }

    @Test
    public void testTrueCredit() {
        arr[0] = 424242424242424L; arr[1] = 2;
        luhn.newNumbers(arr);
        assertTrue(luhn.validate());
    }

    @Test
    public void testWikiCredit() {
        arr[0] = 7992739871L; arr[1] = 3;
        luhn.newNumbers(arr);
        assertTrue(luhn.validate());
    }
    @Test
    public void testLength() {
        arr[0] = 1234567890;
        luhn.newNumbers(arr);
        assertEquals(11,luhn.getLength());
    }

    @Test
    public void testValidate() {
        //This validates on the number 00, that is arr = [0,0];
        assertTrue(luhn.validate());
    }

    @Test
    public void testFalseValidate() {
        arr[0] = 1;
        luhn.newNumbers(arr);
        assertFalse(luhn.validate());
    }

    @Test
    public void testGetCheck() {
        assertEquals(0, luhn.getCheck());
    }

    @Test
    public void testCreditCard() {
        arr[0] = 123456789012345L; arr[1] = 2;
        luhn.newNumbers(arr);
        assertTrue(luhn.validate());
    }
}