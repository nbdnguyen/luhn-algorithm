package main.java;

import java.util.Scanner;

public class Input {
    Scanner scanner = new Scanner(System.in);
    long[] numbers = new long[2];

    public Input() {
        numbers = getCorrectInput();
        scanner.close();
    }

    public long[] getCorrectInput() {
        System.out.print("Input a number to test: ");
        String input = scanner.nextLine();
        long[] current = new long[2];
        int check;
        long number;
        try {
            check = Character.getNumericValue(input.charAt(input.length()-1));
            number = Long.parseLong(input.substring(0,input.length()-1));
            current[0] = number;
            current[1] = check;
        } catch (Exception e) {
            System.out.println("Error: That is not a valid input!");
            System.out.println();
            current = getCorrectInput();
        }
        return current;
    }

    public long[] getNumbers() {
        return numbers;
    }
}
