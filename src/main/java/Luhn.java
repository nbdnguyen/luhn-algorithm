package main.java;

public class Luhn {
    private long[] numbers;
    private int check;
    private int length;

    public Luhn(long[] numbers) {
        this.numbers = numbers;
        check = findCheck();
    }

    public void newNumbers(long[] in) {
        this.numbers = in;
        findCheck();
        validate();

    }
    public boolean validate() {
        if (check == numbers[1]) {
            return true;
        } else {
            return false;
        }
    }

    public int getCheck() {
        return check;
    }

    public int getLength() {
        //Length + check digit.
        return length + 1;
    }

    public int findCheck() {
        String num = Long.toString(numbers[0]);
        char[] nums = num.toCharArray();
        length = nums.length;
        long[] arr = new long[length];
        for (int i=0; i<length; i++) {
            arr[i] = Character.getNumericValue(nums[i]);
        }
        for (int i=length-1; i>=0 ; i=i-2) {
            if (arr[i]*2 >= 10) {
                arr[i] = arr[i]*2 - 9;
            } else {
                arr[i] = arr[i] * 2;
            }
        }

        int sum = 0;
        for (int i=0; i<length; i++) {
            sum += arr[i];
        }

        check = (sum*9%10);
        if (check == 10) {
            check = 0;
        }
        return check;
    }
}
